import { RESTDataSource } from 'apollo-datasource-rest';

export class DockerAPI extends RESTDataSource {
  constructor() {
    super();
    this.baseURL = 'http://127.0.0.1:2376/';
  }

  async getAllContainers() {
    return this.get('containers');
  }

  async getAContainer(Id) {
    const result = await this.get('container', {
      Id
    });

    return result[0];
  }
};