import { ApolloServer, gql } from 'apollo-server';

export const typeDefs = gql`
  type Container {
    Id: String!
    Names: [String!]!
    Image: String!
    ImageID: String!
    Command: String!
    Created: Int!
    State: String!
    Status: String!
    Ports: [Int!]
  }
  type Query {
    container(Id: String!): Container
    containers: [Container]
  }
`;