
import { ApolloServer, gql } from 'apollo-server';
import { DockerAPI } from './datasource';
import { resolvers } from './resolver';
import { typeDefs } from './docker-schema';

const server = new ApolloServer({
  typeDefs,
  resolvers,
  dataSources: () => ({
    dockerAPI: new DockerAPI()
  })
});

server.listen().then(({ url }) => {
  console.log(`🚀 Server ready at ${url}`)
});