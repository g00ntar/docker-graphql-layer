export const resolvers = {
  Query: {
    container: (root, { Id }, { dataSources }) =>
      dataSources.DockerAPI.getAContainer(Id),

    containers: (root, args, { dataSources }) =>
      dataSources.DockerAPI.getAllContainers()
  },
  Container: {
    State: ({ State }) => State
  }
};
